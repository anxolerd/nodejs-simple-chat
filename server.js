"use strict";

let api = {};
global.api = api;
api.net = require('net');
api.ps = require('process');


// chat history
let history = [];
const port = api.ps.argv[2] || 4891;
let connections = [];

const server = api.net.createServer((conn) => {
    connections.push(conn);
    console.log(`Got connection from ${conn.localAddress}`);

    // Send existing history to connection
    conn.write(JSON.stringify({'history': history}));

    conn.on('data', (data) => {
        const parsed = JSON.parse(data);
        if (parsed && parsed.message) {
            history.push(parsed.message);
            connections.map((c) => {
                c.write(JSON.stringify({message: parsed.message}));
            });
        }
    });

    conn.on('close', () => {
        // deregister connection
        const idx = connections.indexOf(conn);
        console.log('Someone disconnected');
        if (idx != -1) connections.splice(idx, 1);
    });
}).listen(port);
