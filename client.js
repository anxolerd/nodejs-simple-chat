"use strict";

let api = {};
global.api = api;
api.net = require('net');
api.ps = require('process');
api.util = require('util');

// chat history
let history = [];
const host = api.ps.argv[2];
const port = api.ps.argv[3];
if (!(host && port)) {
    console.error("No server url!");
    api.ps.exit(1);
}

const username = api.ps.argv[4] || "Big brother";

function print_message(message) {
    const username = message.username;
    const text = message.text;
    console.log(`${username}: ${text}`);
}

const socket = new api.net.Socket();
socket.connect({
    host: host,
    port: port,
}, () => {
    socket.on('data', (data) => {
        const parsed = JSON.parse(data);
        if (!parsed) return;
        if (parsed.history) {
            history = parsed.history;
            history.map(print_message);
        } else if (parsed.message) {
            history.push(parsed.message);
            print_message(parsed.message);
        };
    });

    socket.on('end', () => {
        console.log('server died.');
        api.ps.exit(0);
    });
});

// Set up REPL
api.ps.stdin.resume();
api.ps.stdin.setEncoding('utf-8');
api.ps.stdin.on('data', (text) => {
    if (text === '\\q\n') {
        console.log('Bye');
        api.ps.exit(0);
    }

    socket.write(JSON.stringify({
        message: {
            username: username,
            text: text.slice(0,-1),
        }
    }));
});
